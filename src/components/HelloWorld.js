import React from 'react'

import '../assets/styles/HelloWorld.scss'

class HelloWorld extends React.Component {
    render() {
        return (
            <div>
                <h1>
                    Hello, World!!!
                    <small>black</small>
                </h1>
            </div>
        )
    }
}

export default HelloWorld